﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTaken : MonoBehaviour
{
    [SerializeField] Canvas splatterCanvas;
    [SerializeField] float splatterTime = 0.5f;


    // Start is called before the first frame update
    void Start()
    {
        splatterCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowHitImpact()
    {
        StartCoroutine(ShowSplatter());
    }

    IEnumerator ShowSplatter()
    {
        splatterCanvas.enabled = true;
        yield return new WaitForSeconds(splatterTime);
        splatterCanvas.enabled = false;
    }
}
