﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler : MonoBehaviour
{
    [SerializeField] Canvas GameOverCanvas;

    private void Start()
    {
        GameOverCanvas.enabled = false;
    }

    public void HandleDeath()
    {
        GameOverCanvas.enabled = true;
        Time.timeScale = 0; // freeze time upon death
        FindObjectOfType<WeaponSwitcher>().enabled = false; // disable weapon switcher on death
        Cursor.lockState = CursorLockMode.None; // enables cursor movement
        Cursor.visible = true;
    }

}
