﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class WeaponZoom : MonoBehaviour
{
    [SerializeField] Camera FPCamera;
    [SerializeField] RigidbodyFirstPersonController fpsController;
    [SerializeField] float FOVZoomedIn = 40F;
    [SerializeField] float FOVZoomedOut = 60f;
    [SerializeField] float zoomInSensitivity = 1f;
    [SerializeField] float zoomOutSensitivity = 2f; // XandY axis sensitivity can be used for more custom


    bool isZoomed = false;

    private void OnDisable()
    {
        ZoomOut();
    }

    // Start is called before the first frame update
    void Start()
    {
        fpsController = GetComponent<RigidbodyFirstPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            Zoom();
        }
    }

    private void Zoom()
    {
        // FPCamera.fieldOfView = FOVZoomedIn;
        if (isZoomed == false)
        {
            ZoomIn();
        }
        else
        {
            ZoomOut();
        }
    }

    private void ZoomIn()
    {
        isZoomed = true;
        FPCamera.fieldOfView = FOVZoomedIn;
        fpsController.mouseLook.XSensitivity = zoomInSensitivity;
        fpsController.mouseLook.YSensitivity = zoomInSensitivity;
    }

    private void ZoomOut()
    {
        isZoomed = false;
        FPCamera.fieldOfView = FOVZoomedOut;
        fpsController.mouseLook.XSensitivity = zoomOutSensitivity;
        fpsController.mouseLook.YSensitivity = zoomOutSensitivity;
    }
}
