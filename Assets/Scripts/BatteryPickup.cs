﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPickup : MonoBehaviour
{
    [SerializeField] float restoreAngle = 50f;
    [SerializeField] float addIntensity = 8f;

    void Start()
    {
        GetComponentInChildren<FlashlightSystem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameObject.FindWithTag("Player")) // or (other.gameObject.tag == "Player")
        {
            // Debug.Log("Player got batteries");
            other.GetComponentInChildren<FlashlightSystem>().RestoreLightAngle(restoreAngle);
            other.GetComponentInChildren<FlashlightSystem>().AddLightIntensity(addIntensity);
            Destroy(gameObject);
        }
    }
}
