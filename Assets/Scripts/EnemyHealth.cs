﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] float hitPoints = 100f;

    bool isDead = false;

    public bool IsDead()
    {
        return isDead;
    }

    // create public method which reduces hitPoints by amount of damage

    public void TakeDamage(float damage)
    {
        // GetComponent<EnemyAI>().OnDamageTaken(); method 1
        BroadcastMessage("OnDamageTaken"); // string reference

        hitPoints -= damage;
        if (hitPoints <= 0)
        {
            // Destroy(gameObject);
            Die();
        }
    }

    private void Die()
    {
        if (isDead) return;
        isDead = true;
        GetComponent<Animator>().SetTrigger("Die");
    }
}
